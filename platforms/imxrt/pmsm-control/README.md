This is a control of PMSM (permanent magnet sychronous motor) control application designed in pysimCoder tool for Teensy 4.1 Base Board.

![Electrical Connection](figs/pmsm-electric.png "Electrical Connection")

### Compilation:

Command ./build [shv] [clean] takes care of pysimCoder compilation and build. Optional parameter shv adds support for Silicon Heaven
infrastructure. The command opens pysimCoder GUI application with a designed block diagram. The diagram is converted to C code
with Genetate C-Code button. Executable rc_plant is created after that.

Code can be flashed to the board with Teensy Loader application (https://www.pjrc.com/teensy/loader_cli.html):

> teensy_loader_cli --mcu=TEENSY41 -v -w pmsm_control.hex
