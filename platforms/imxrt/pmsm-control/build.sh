#!/bin/bash

if [[ "$*" == "clean" ]]
then
  echo "Removing directory pysimCoder."
  rm -r -f nuttx-export nuttx-export-12.1.0 pysimCoder/ pmsm_control_gen/ pmsm_control.elf pmsm_control pmsm_control.hex
  exit 1
fi

git clone https://github.com/robertobucher/pysimCoder.git
#git clone https://github.com/apache/incubator-nuttx.git nuttx
#git clone https://github.com/apache/incubator-nuttx-apps.git apps

export PYSUPSICTRL="$PWD"/pysimCoder

tar -xvf nuttx-export-12.1.0.tar.gz
mv nuttx-export-12.1.0 nuttx-export
mv nuttx-export pysimCoder/CodeGen/nuttx
cd pysimCoder/CodeGen/nuttx/devices
if [[ "$*" == "shv" ]]
then
  echo "SHV"
  make SHV=1
else
  make
fi
cd ..
cd ..
cd ..
cd ..

./pysimCoder/pysim-run.sh pmsm_control.dgm
