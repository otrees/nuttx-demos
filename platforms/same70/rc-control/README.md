This is a RC Plant (resistor-capacitor) control application designed in pysimCoder tool for SAM E70 MCU based board SAM E70 Xplained.

Following pins are used on the board:

- AD3 for ADC1 channel 0 input
- D7 for PWM1 channel 1 output

![Electrical Connection](figs/rcplant-electric.png "Electrical Connection")

### Compilation:

Command ./build [shv] [clean] takes care of pysimCoder compilation and build. Optional parameter shv adds support for Silicon Heaven
infrastructure. The command opens pysimCoder GUI application with a designed block diagram. The diagram is converted to C code
with Genetate C-Code button. Executable rc_plant is created after that.

SAM E70 Xplained requires the executable to be a binary, the conversion is following:

> arm-none-eabi-objcopy -O binary rc_plant rc_plant.bin

The executable can be flashed to the board with OpenOCD (use same70-openocd configuration).

