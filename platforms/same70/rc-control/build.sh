#!/bin/bash

if [[ "$*" == "clean" ]]
then
  echo "Removing directory pysimCoder."
  rm -r -f nuttx-export pysimCoder/ rc_plant_gen/ rc_plant.elf rc_plant rc_plant.bin
  exit 1
fi

git clone https://github.com/robertobucher/pysimCoder.git

export PYSUPSICTRL="$PWD"/pysimCoder

cd pysimCoder
cd ..
unzip nuttx-export.zip
mv nuttx-export pysimCoder/CodeGen/nuttx
cd pysimCoder/CodeGen/nuttx/devices
if [[ "$*" == "shv" ]]
then
  echo "SHV"
  make SHV=1
  make SHV=1
else
  make
fi
cd ..
cd ..
cd ..
cd ..

./pysimCoder/pysim-run.sh rc_plant.dgm
